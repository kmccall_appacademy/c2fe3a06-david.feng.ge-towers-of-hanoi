# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  def initialize(height = 3)
    @towers = [[], [], []]
    height.downto(1).each { |disc| @towers[0] << disc }
  end

  attr_reader :towers

  def move(from_tower, to_tower)
    disc_moved = towers[from_tower].pop
    towers[to_tower].push(disc_moved)
  end

  def valid_move?(from_tower, to_tower)
    if towers[from_tower].empty?
      return false
    elsif towers[to_tower].empty?
      return true
    elsif towers[from_tower].last > towers[to_tower].last
      return false
    end
    true
  end

  def won?
    towers[0].empty? && ( towers[1].empty? || towers[2].empty? )
  end

  def render
    p "-----------"
    towers.each do |tower|
      tower.each do |disc|
        print disc
        print ' '
      end
      print "\n"
    end
    p "-----------"
  end

end

def play
  puts "Welcome to the Towers of Hanoi"
  puts "Please input the height of the tower"
  height = gets.chomp
  game = TowersOfHanoi.new(height.to_i)
  game.render
  until game.won?
    puts "Move disc from tower #?"
    from_tower = gets.chomp
    puts "Move disc to tower #?"
    to_tower = gets.chomp
    until game.valid_move?(from_tower.to_i, to_tower.to_i)
      puts "Invalid move, please try again."
      puts "Move disc from tower #?"
      from_tower = gets.chomp
      puts "Move disc to tower #?"
      to_tower = gets.chomp
      # solution: from_tower, to_tower = input.split.map {&:to_i}
    end
    game.move(from_tower.to_i, to_tower.to_i)
    game.render
  end
  puts "You won!"
end

# in terminal, use ruby filename.rb to run this part of code
if __FILE__ == $PROGRAM_NAME
  play
end
